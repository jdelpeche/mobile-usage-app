package com.GMPLL.getprocess;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.Service;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Environment;
import android.os.IBinder;
import android.text.format.Time;

public class MyService extends Service {
	Run thread;
	File path = Environment
			.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
	Calendar Now = Calendar.getInstance();
	File file;
	File trans;
	ApplicationInfo ai;
	PackageManager pm;
	ArrayList<String> transistion = new ArrayList<String>();
	OutputStream os = null;
	OutputStream ts = null;

	@Override
	public void onCreate() {
		pm = getPackageManager();
		thread = new Run();
		file = new File(path, "appLog" + Now.get(Calendar.DAY_OF_YEAR)
				+ Now.get(Calendar.HOUR_OF_DAY) + Now.get(Calendar.MINUTE)
				+ Now.get(Calendar.SECOND) + ".txt");
		trans = new File(path, "transition" + Now.get(Calendar.DAY_OF_YEAR)
				+ Now.get(Calendar.HOUR_OF_DAY) + Now.get(Calendar.MINUTE)
				+ Now.get(Calendar.SECOND) + ".txt");
		try {
			ts = new FileOutputStream(trans, true);
			os = new FileOutputStream(file, true);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public class Run extends Thread {
		private boolean Loop = true;

		@Override
		public void run() {
			StringBuilder t = new StringBuilder();
			StringBuilder b = new StringBuilder();
			path.mkdirs();
			while (Loop) {

				ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
				List<RunningTaskInfo> runningTask = manager.getRunningTasks(1);
				String Times;
				Time today = new Time(Time.getCurrentTimezone());
				today.setToNow();
				Times = today.format("%k:%M:%S");
				Map<String, String> PList = new HashMap<String, String>();
				for (RunningTaskInfo i : runningTask) {
					try {
						ai = pm.getApplicationInfo(
								i.topActivity.getPackageName(), 0);
					} catch (NameNotFoundException e) {
						ai = null;
					}
					String applicationName = (String) (ai != null ? pm
							.getApplicationLabel(ai) : "(unknown)");
					if (!applicationName.equals("GetProcess")
							&& !applicationName.equals("Launcher")
							&& !applicationName.contains("System")) {
						PList.put(applicationName, Times);
						switch (transistion.size()) {
						case 0:
							transistion.add(applicationName);
							break;
						case 1:
							if (!transistion.get(0).equalsIgnoreCase(
									applicationName))
								transistion.add(applicationName);
							break;
						}
					}
				}
				for (Map.Entry<String, String> entry : PList.entrySet()) {
					b.append(entry.getKey() + ";" + entry.getValue());
					b.append("\n");
				}

				try {

					if (transistion.size() == 2) {
						
						t.append(transistion.get(0) + "," + transistion.get(1));
						t.append("\n");
						ts.write(t.toString().getBytes());
						t.setLength(0);
						transistion.remove(0);
						
					}

					// b.append(file.toString());
					os.write(b.toString().getBytes());
					b.setLength(0);

				} catch (Exception e) {
					e.printStackTrace();
				}

				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}

		public void setLoop(boolean loop) {
			Loop = loop;
		}

	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onDestroy() {

		try {
			os.close();
			ts.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		thread.setLoop(false);

	}

	public int onStartCommand(Intent intent, int flags, int startId) {
		if (!thread.isAlive()) {
			thread.start();
		}
		return START_REDELIVER_INTENT;
	}

}
