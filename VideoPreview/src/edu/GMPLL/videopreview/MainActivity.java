package edu.GMPLL.videopreview;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String path = "http://commonsware.com/misc/test2.3gp";
        MediaController mediaController = new MediaController(this);
        
        VideoView video = (VideoView)findViewById(R.id.Video);
        video.setMediaController(mediaController);
        
        Uri uri = Uri.parse(path);
        video.setVideoURI(uri);
        video.start();
    }


    
}
